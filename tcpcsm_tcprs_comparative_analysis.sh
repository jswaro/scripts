#!/bin/bash

TCPRS_RETRANSMISSION="tcpretransmissions.log"
TCPRS_REORDERING="tcpreordering.log"

# makes the comparative samples for a single classification
make_comparative_samples() {
	classification=$1
	base_directory=$2

	if [[ ! -d $base_directory/results/$classification ]] ; then
		echo "error: classification directory does not exist - $base_directory/results/$classification"
                exit 1
        fi
        cd $base_directory/results/$classification

        tcprs_record_file=$TCPRS_RETRANSMISSION
        if [[ $classification == "REORDER" ]] ; then
	        tcprs_record_file=$TCPRS_REORDERING
        fi

	#copy the record file into orig.log where it is used later
	ORIGINAL_FILE=$base_directory/tcprs/$tcprs_record_file
	if [[ ! -f $ORIGINAL_FILE ]] ; then
		echo "Error: could not find the original record file - $ORIGINAL_FILE"
		return
	fi
	
	cp $ORIGINAL_FILE orig.log

        tcprs_record_file="$base_directory/tcprs/$tcprs_record_file"
        if [[ ! -f $tcprs_record_file ]] ; then
	        echo "warning: could not locate tcprs record file for $classification processing - $tcprs_record_file"
                return
        fi

	if [[ ! -f tcprs.ldr ]] ; then
		echo "error: could not find tcprs.ldr file in directory - $PWD"
	fi

	# split the tcprs.ldr file into $EVENT.ldr files
	split_tcprs_ldr.py tcprs.ldr
	 
	generate_tcpcsm_comparative_samples.sh $classification


}

main() {
	tracefile=$(readlink -f $1)
	dest=$2
	
	if [[ ! -d $dest ]] ; then
		mkdir -p $dest
	fi
	
	dest=$(readlink -f $2)

	workingdir=$PWD

	if [[ ! -f $tracefile ]] ; then 
		echo "warning: tracefile does not exist - $tracefile"
		exit 1
	fi

	if [[ ! -d $dest ]] ; then
		echo "making analysis directory at $dest"
		mkdir -p $dest
	fi

	generate_raw_output.sh $tracefile $dest
	compare_reordering.sh $dest $tracefile
	
	if [[ ! -d $dest/results ]] ; then
		echo "error: results directory does not exist - $dest/results"
		exit 1
	fi
	
	if [[ ! -d $dest/tcprs ]] ; then
		echo "error: tcprs results directory does not exist -- $dest/tcprs"
		exit 1
	fi
	
	cd $dest/results

	# make the samples for each of the respective classifications
	for classification in FRETX REORDER RTO ; do 
		make_comparative_samples $classification $dest
		cd $dest/results
	done

}

usage() {
	me=`basename $0`
        echo "Usage: $me \<tracefile\> \<destination_directory\>"
}

# start of program
args=$#

if [[ "$args" -eq "2" ]]
then
        main $1 $2
else
        usage
fi
