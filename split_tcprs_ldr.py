#!/usr/bin/env python
# encoding: utf-8
'''
split_tcprs_ldr -- shortdesc

split_tcprs_ldr is a description

It defines classes_and_methods

@author:     user_name

@copyright:  2014 organization_name. All rights reserved.

@license:    license

@contact:    user_email
@deffield    updated: Updated
'''

import sys
import os

from optparse import OptionParser

__all__ = []
__version__ = 0.1
__date__ = '2014-01-24'
__updated__ = '2014-01-24'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

def main(argv=None):
    '''Command line options.'''

    program_name = os.path.basename(sys.argv[0])
    program_version = "v0.1"
    program_build_date = "%s" % __updated__

    program_version_string = '%%prog %s (%s)' % (program_version, program_build_date)
    program_usage = "usage: {0} <ldr_file>".format(program_name)
    program_longdesc = '''''' # optional - give further explanation about what the program does
    program_license = "Copyright 2014 user_name (organization_name)                                            \
                Licensed under the Apache License 2.0\nhttp://www.apache.org/licenses/LICENSE-2.0"

    if argv is None:
        argv = sys.argv[1:]
        # setup option parser
        
    parser = OptionParser(version=program_version_string, epilog=program_longdesc, description=program_license, usage=program_usage)
    parser.add_option("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %default]")
    parser.add_option("-d", "--dir", dest="dir", action="store", help="set output directory (default: %default )", default=os.path.abspath("."))
                      

    # process options
    (opts, args) = parser.parse_args(argv)
    
    if len(args) < 1:
        parser.print_help()
        sys.exit(1)
        
    ldr_file = os.path.abspath(os.path.expanduser(args[0]))

    if opts.verbose > 0:
        print("verbosity level = %d" % opts.verbose)


    split_ldr_file(ldr_file, opts.dir)
    
class record(object):
    def __init__(self):
        self.tcprs_events = []
        self.tcpcsm_events = []
        self.tcpdump_line = ""
        self.tag = None

def split_ldr_file(ldr_file, output_dir):
    
    events = {}
    with open(ldr_file, 'r') as log: 
        ''' LDR HEADER FORMAT
        #!/bin/bash
        #< event in tcprs
        #> event in tcpcsm
        # tcpdump command
        if [[ ! -d /media/research/research/analysis/google/results/FRETX/traces ]] ; then mkdir /media/research/research/analysis/google/results/FRETX/traces ; fi
        if [[ ! -d /media/research/research/analysis/google/results/FRETX/plots ]] ; then mkdir /media/research/research/analysis/google/results/FRETX/plots ; fi
        '''
        
        header = ""
        ''' strip the file header '''
        for x in xrange(0, 6):
            header += log.readline()
        
        ''' LDR RECORD FORMAT 
        #< event in tcprs  
        #> event in tcpcsm 
        tcpdump command
        '''
        rec = None
        
        for line in log:
            l = line.strip()
            if line == "":
                continue
            
            if line.startswith("#<"):
                ''' tcprs line '''
                rec = record()
                
                rec.tcprs_events.append(l)
            elif line.startswith("#>"):
                ''' tcpcsm line '''
                rec.tcpcsm_events.append(l)
                
                ''' tag exists at the 7th index of the line ''' 
                rec.tag = l.split()[7]
            elif line.startswith("/usr/sbin/tcpdump"):
                ''' tcpdump line '''
                
                rec.tcpdump_line = l
                
                ''' end of record '''
                if rec.tag is None:
                    rec.tag = "None"

                if rec.tag not in events:
                    events[rec.tag] = []
                events[rec.tag].append(rec)

                assert(len(rec.tcprs_events) != 0)
                
                rec = None
            elif l == "":
                continue
            else:   
                print >> sys.stderr, "wierd record line: ", l
                    
    for e_type in events:
        with open(os.path.join(output_dir, "{0}.ldr".format(e_type)), 'w') as output_log:
            print >> output_log, header
            for rec in events[e_type]:
                print >> output_log, "\n".join(rec.tcprs_events)
                print >> output_log, "\n".join(rec.tcpcsm_events)
                print >> output_log, rec.tcpdump_line
                print >> output_log, ""
            
        
    return
    

if __name__ == "__main__":
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'split_tcprs_ldr_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())