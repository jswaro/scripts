#!/bin/bash
#
# The purpose of this script is to take a entire classification of tcprs
# events and generate all of the tcpcsm event samples
#

#3735928559 is 0xDEADBEEF
SEED=3735928559
RANDOM=${SEED}

usage () {
	me=`basename $1`
	echo "Usage: $me <tcprs_classification>"
}

generate_samples () {
	filename=`basename $1`
	working_dir=$PWD
	classification=`echo $filename | awk -F. '{print $1}'`

	if [[ ! -d $classification ]] ; then
		mkdir $classification
	fi
	
	cp orig.log $classification/orig.log
	cp $filename $classification/$filename	
	cd $classification
	
	if [[ -f log ]] ; then
		rm log
	fi
	samples=${classification}_samples.sh
	
	#make script to generate samples
	echo "making samples using ${filename}, writing to ${samples} using seed ${SEED}"
	echo get_random_samples_from_ldr.py -i ${filename} -o ${samples} -s ${SEED}
	get_random_samples_from_ldr.py -i ${filename} -o ${samples} -s ${SEED} 2>&1 >> log

	chmod 755 ${samples}
	
	#generate samples
	results=$(bash ${samples} 2>&1)
	echo $results >> log
	
	#generate events list for annotating
	echo "filtering events specific to the samples"
	grep ^"#<" $samples > event.log 
	results=$(make_event_filter_from_log.sh event.log)
	echo $results >> log
	
	#annotate all of the xplot files 
	echo "annotating xplot samples"
	annotate 2>&1 >> log
	
	cd $working_dir
}

args=$#

if [[ $args -ne 1 ]] ; then
	usage $0
	exit 1
fi

#for each of the events given below, the filter corresponds to event
#  types that are relevant to the comparative analysis
cls=$1 
case $cls in 
	"FRETX")
		ldr_filter="RTO LOSS_REC FREC UNEXP_FREC REORDER LINK_DUP IPID_DUP None"
		;;
	"RTO")
		ldr_filter="FRETX MS_FRETX SACK_FRETX BAD_FRETX LOSS_REC FREC UNEXP_FREC REORDER LINK_DUP IPID_DUP None"
		;;
	"REORDER")
		ldr_filter="RTO FRETX MS_FRETX SACK_FRETX BAD_FRETX LOSS_REC FREC UNEXP_FREC LINK_DUP IPID_DUP None"
		;;
	*)
		echo "Unrecognized classification... exiting"
		exit 1
		;;
esac

for each in $ldr_filter ; do 
	ldr_file=$each.ldr
	if [[ -f $ldr_file ]] ; then 
		echo "generating samples for $each in $PWD/$each"
		generate_samples $ldr_file
	fi
done
	

	
