#!/bin/bash

main() {
	tracefile=$(readlink -f $1)
	dest=$2
	
	if [[ ! -d $dest ]] ; then
		mkdir -p $dest
	fi
	
	dest=$(readlink -f $2)

	workingdir=$PWD

	if [[ ! -f $tracefile ]] ; then 
		echo "warning: tracefile does not exist - $tracefile"
		exit 1
	fi

	if [[ ! -d $dest ]] ; then
		echo "making analysis directory at $dest"
		mkdir -p $dest
	fi

	time generate_raw_output.sh $tracefile $dest
	time compare_reordering.sh $dest $tracefile
}

usage() {
	me=`basename $0`
	echo "Usage: $me <tracefile> <destination_directory>"
}

# start of program
args=$#

if [[ "$args" -eq "2" ]]
then
        main $1 $2
else
        usage
fi
