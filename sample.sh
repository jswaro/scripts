#!/bin/bash

count=`grep ^"/usr" $1 | wc -l`
max_count=100
if [[ $count -lt $max_count ]] ; then
	let freq=1
else
	let freq=$count/$max_count
fi
grep ^"/usr" $1 > .tmp

events=0
echo 'rm ../traces/*'
while [[ $events -lt $max_count ]] ; do 
	read line
	x=`expr $RANDOM % $freq`
	if [[ $x -eq 0 ]] ; then
		if [[ "$line" == "" ]] ; then
			break
		fi
		echo $line
		paddednum=`printf "%03d" $events`
		echo mv ../traces/\*.dmp $paddednum.trace
		let events=$events+1
	fi	
done < '.tmp'
echo 'for x in *.trace; do tcptrace --output_prefix=$x. --xplot_title_prefix=$x -nS $x ; done' 
