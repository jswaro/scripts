#/bin/bash

for XPL_FILE in $@ ; do 
	ID=`egrep [0-9]+.trace $XPL_FILE | tr :_ '\t' | awk '{print $2, $5, $3, $6}'`
	ALT=`egrep [0-9]+.trace $XPL_FILE | tr :_ '\t' | awk '{print $5, $2, $6, $3}'`
	expected_events=`grep "$ID" event.log | wc -l`
	if [[ $expected_events -eq 0 ]] ; then 
		ID=$ALT
		expected_events=`grep "$ID" event.log | wc -l`
	fi

	found_events=`grep $XPL_FILE results.txt | wc -l`

	if [[ ${expected_events} -ne ${found_events} ]] ; then 
		echo "found ${found_events} events but expected ${expected_events} events for ${XPL_FILE} - $ID"
	fi
done



