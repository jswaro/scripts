#!/usr/bin/awk -f 

BEGIN {
	connections = 0
	retransmits = 0
	reorderings = 0
	conns_with_retransmits = 0
	conns_with_reorderings = 0
}

{
	if ($1 == "TCP" && $2 == "connection") {
		connections += 1;
	} else if ($1 == "rexmt" && $2 == "data" && $3 == "pkts:") {
		count = $4 + $8
		retransmits += count
		if (count > 0)
			conns_with_retransmits += 1
	} else if ($1 == "outoforder" && $2 == "pkts:") {
		count = $3 + $6
		reorderings += count
		if (count > 0)
			conns_with_reorderings += 1
	}
}

END {
	print ("connections: " connections)
	print ("retransmits: " retransmits)
	print ("reorderings: " reorderings)
	print ("conns_with_reorderings: " conns_with_reorderings)
	print ("conns_with_retransmits: " conns_with_retransmits)
}
