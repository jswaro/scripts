#!/bin/bash

usage() {
	me=`basename $0`
	echo "Usage: $me <xplot files ...> "
	echo ""
	echo "    This utility opens and closes xplot files then writes yes"
	echo "    or no to results.txt based on user input of 'nNyY'"
}


args=$#

if [[ $args -lt 1 ]] ; then
	usage
	exit 1
fi


if [[ -f results.txt ]] ; then
	rm -f results.txt
fi


for xplot in $@ ; do 
	echo "opening $xplot"
	xplot $xplot 2>&1 > /dev/null 
	echo "Does xplot exhibit behavior as analyzed by TCPRS? [N/y]"
	read answer
	
	valid=0
	while [[ valid -eq 0 ]] ; do
		valid=`echo $answer | awk 'BEGIN{valid=1} {for(i=1;i<=NF;i++) {if ($i != "n" && $i != "N" && $i != "y" && $i != "Y") {valid = 0;}}} END{ print valid}'`
		
		if [[ $valid -eq 0 ]] ; then 
			echo "\"$answer\" is an invalid response... try again"
	
			read answer
			continue
		else 
			echo $answer | awk -v xplot="$xplot" '{for(i=1;i<=NF;i++) {if ($i == "n" || $i == "N") { print xplot, "N"} else {print xplot, "Y"} } }' >> results.txt
		fi

	done
done 

