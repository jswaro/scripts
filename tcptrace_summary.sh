#!/bin/bash 
main() {
	tracesummary=$(readlink -f $1)

	grepprog="grep"
	egrepprog="egrep"	

	if [ ${tracesummary: -3} == ".gz" ] ; then
	        zcat $tracesummary | egrep [0-9]+[[:space:]]"TCP connections traced:" | awk '{print "connections:",$1}' > $tracesummary.summary
	        zcat $tracesummary | grep "rexmt data pkts" | awk 'BEGIN{x=0} {x+=$4 + $8} END{print "retransmits:",x}' >> $tracesummary.summary
        	zcat $tracesummary | grep "outoforder pkts" | awk 'BEGIN{x=0} {x+=$3 + $6} END{print "reorderings:",x}' >> $tracesummary.summary
	else
	        egrep [0-9]+[[:space:]]"TCP connections traced:" $tracesummary | awk '{print "connections:",$1}' > $tracesummary.summary
	        grep "rexmt data pkts" $tracesummary | awk 'BEGIN{x=0} {x+=$4 + $8} END{print "retransmits:",x}' >> $tracesummary.summary
        	grep "outoforder pkts" $tracesummary | awk 'BEGIN{x=0} {x+=$3 + $6} END{print "reorderings:",x}' >> $tracesummary.summary
	fi	

}

usage() {
        echo "Usage: tcptrace_summary <tracefile_summary>"
}

# start of program
args=$#

if [[ "$args" -eq "1" ]]
then
        main $1
else
        usage
fi
