#!/usr/bin/awk -f 

BEGIN {
	connections=0;
	retransmits=0;
	reorderings=0;
	conns_with_reorderings = 0;
	conns_with_retransmits = 0;
}

{
	if ($1 == "connections:") {
		connections += $2
	} else if ($1 == "retransmits:") {
		retransmits += $2
	} else if ($1 == "reorderings:") {	
		reorderings += $2
	} else if ($1 == "conns_with_reorderings:") {
		conns_with_reorderings += $2	
	} else {
		conns_with_retransmits += $2
	}
}

END {
	print ("connections:" connections)
	print ("retransmits:" retransmits)
	print ("reorderings:" reorderings)
	print ("conns_with_reorderings: " conns_with_reorderings)
	print ("conns_with_retransmits: " conns_with_retransmits)
}
