#!/usr/bin/python

import sys
import re
from optparse import OptionParser

class connection:
	def __init__(self):
		self.src_h = None;
		self.src_p = None;
		self.dst_h = None;
		self.dst_p = None;
		self.src_rexmt = 0;
		self.dst_rexmt = 0;
		self.src_reord = 0;
		self.dst_reord = 0;

	def toString(self):
		ret = "{0} {1} {2} {3} {4} {5} {6} {7}".format(
			self.src_h, self.src_p, self.dst_h, self.dst_p,
			self.src_rexmt, self.dst_rexmt, self.src_reord, self.dst_reord);
		return ret;
		
	def dumpString(self, tracefile):
		ret = "/usr/sbin/tcpdump -r {0} -w {1}_{2}_{3}_{4}.dmp 'host {1} and host {3} and port {2} and port {4}'".format(
			tracefile, self.src_h, self.src_p, self.dst_h, self.dst_p);
		return ret;

def init_parser():
    usage="usage: %prog [options] tcpretransmission.log tcpreordering.log tracefile"
    options_list = [];
    parser = OptionParser(usage=usage, option_list=options_list);
    return parser;
    
def main():
	parser = init_parser();

	(options, args) = parser.parse_args();

	record_re = re.compile("^[0-9]+");

	rexmt_log = None;
	reord_log = None;

	if(len(args) != 2):
		parser.print_help();
		exit(-1)
		
	try:
		rexmt_log = open(args[0]);
	except IOError :
		print "could not open {0}".format(args[0])
		exit(1);

	try:
		reord_log = open(args[1]);
	except IOError:
		print "could not open {0}".format(args[1])
		exit(1);

	c = None;
	connections = {};

	for line in rexmt_log:
		if (not record_re.search(line)):
			continue;
			
		fields = line.split();
		uid = fields[1];

		if (uid not in connections):
			c = connection();
			c.src_h = fields[2];
			c.src_p = fields[3];
			c.dst_h = fields[4];
			c.dst_p = fields[5];
			connections[uid] = c;
		else:
			c = connections[uid]
			
		if (fields[10] == "T"): #src to dst retransmission
			c.src_rexmt += 1;
		else:
			c.dst_rexmt += 1;
			
	for line in reord_log:
		if (not record_re.search(line)):
			continue;
			
		fields = line.split();
		uid = fields[1];
		
		
		if (uid not in connections):
			c = connection();
			c.src_h = fields[2];
			c.src_p = fields[3];
			c.dst_h = fields[4];
			c.dst_p = fields[5];
			connections[uid] = c;
		else:
			c = connections[uid]
			
		if (fields[11] == "T"): #src to dst reordering
			c.src_reord += 1;
		else:
			c.dst_reord += 1;
			

	for uid in connections:
		c = connections[uid];
		print "# {0}".format(c.toString());
		#print c.dumpString(tracefile);

if __name__ == '__main__':
	main();





