#!/bin/bash

args=$#

if [[ $args -lt 1 ]] ; then
	echo not enough arguments
	exit
fi

rm filter.log
touch filter.log
orig=orig.log

head -n8 $orig > filter.log

for each in `grep ^"#<" $1 | awk '{print $7"[[:space:]].*[[:space:]]"$3"[[:space:]]"$5"[[:space:]]"$2"[[:space:]]"$4}'` ; do 
  egrep $each orig.log >> filter.log
done

head -n8 filter.log > new_log.log 
for each in `grep "#<" $1 | awk '{print $7}'` ; do 
  echo $each 
  grep $each filter.log  >> new_log.log
done 
tail -n1 $orig >> new_log.log 
mv new_log.log tcpretransmissions.log
