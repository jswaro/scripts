#!/usr/bin/python

import sys;
import re;

class connection:
	def __init__(self):
		self.src_h = None;
		self.src_p = None;
		self.dst_h = None;
		self.dst_p = None;
		self.src_rexmt = None;
		self.dst_rexmt = None;
		self.src_reord = None;
		self.dst_reord = None;

	def toString(self):
		ret = "{0} {1} {2} {3} {4} {5} {6} {7}".format(
			self.src_h, self.src_p, self.dst_h, self.dst_p,
			self.src_rexmt, self.dst_rexmt, self.src_reord, self.dst_reord);
		return ret;
		
	def dumpString(self, tracefile):
		ret = "/usr/sbin/tcpdump -r {0} -w {1}_{2}_{3}_{4}.dmp 'host {1} and host {3} and port {2} and port {4}'".format(
			tracefile, self.src_h, self.src_p, self.dst_h, self.dst_p);
		return ret;

def main():

	rexmt_re = re.compile("rexmt data pkts:");
	reord_re = re.compile("outoforder pkts:");
	host_re = re.compile("host [a-zA-Z]+:");
	trace_re = re.compile("filename:");
	
	conn_id = "";
	
	tracefile = None;
	c = connection();

	for line in sys.stdin:
		if (tracefile is None and trace_re.search(line)):
			tracefile = line.split()[1].strip();
			
		if (host_re.search(line)):
			if (c.src_h is not None and c.dst_h is not None):
				print "# {0}".format(c.toString());
				#print c.dumpString(tracefile);
				
				c = connection();
			
			fields = line.strip().split(":");
			host = fields[1].strip();
			port = fields[2].strip();
			if (c.src_h is None):
				c.src_h = host;
				c.src_p = port;
			elif (c.dst_h is None):
				c.dst_h = host;
				c.dst_p = port;
			else:
				raise Exception("This should not happen");
				
		elif (rexmt_re.search(line)):
			if(c.src_rexmt is not None):
				raise Exception("This should not happen");
			
			fields = line.split();
			c.src_rexmt = fields[3];
			c.dst_rexmt = fields[7];
		elif (reord_re.search(line)):
			if(c.src_reord is not None):
				raise Exception("This should not happen");
				
			fields = line.split();
			c.src_reord = fields[2];
			c.dst_reord = fields[5];
		else:
			continue;

if __name__ == '__main__':
    main();
