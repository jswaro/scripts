from optparse import OptionParser, make_option
from sys import exit
from decimal import Decimal

def print_to_file(dict,f):
    for each in dict:
        for ts in dict[each]:
            f.write("{0} REORDER {1}\n".format(each, str(ts)));
    return;

''' If the time-delta between two timestamps is less than 20 microseconds,
    assume that TCPRS and TCPCSM are talking about the same event ''' 
def compare_lists(l_list, r_list):
    ll = [];
    rl = [];
    
    for ts in l_list:
        closest = None; 
        dt = None;
        for x in r_list:
            if (dt is None or abs(ts - x) < dt):
                dt = abs(ts - x);
                closest = x;
        if (dt < 0.00002):
            ll.append(ts);
            rl.append(closest);
                
    return (ll,rl)

def read_diff_record(infile):
    dict = {};
    for record in infile:
        fields = record.strip().split();
        flow_id = " ".join(fields[0:4]);
        event_label = fields[4];
        timestamp = fields[5];
        
        if (flow_id not in dict):
            dict[flow_id] = [];
            
        dict[flow_id].append(Decimal(timestamp));
        
    return dict

def init_parser():
    usage="usage: %prog [options] logfile1 logfile2"
    options_list = [];
    parser = OptionParser(usage=usage, option_list=options_list);
    return parser;

def main():
    parser = init_parser();
    
    (options, args) = parser.parse_args();
    
    left = None;
    right = None;
    
    if(len(args) != 2):
        parser.print_help();
        exit(-1)
    try:
        left = open(args[0]);
    except IOError:
        print "could not open {0}".format(args[0])
        exit(1);
        
    try:
        right = open(args[1]);
    except IOError:
        print "could not open {0}".format(args[1])
        exit(1);
    
    outf1 = open(args[0] + ".dfr","w");
    outf2 = open(args[1] + ".dfr","w");
    inspect = open(args[0] + ".overlap", "w");
    same_event = open(args[0] + ".same", "w");
    
    left_dict = read_diff_record(left);
    right_dict = read_diff_record(right);
    
    left_diffs = {};
    right_diffs = {};
    overlap = {};
    same_diffs = {};
    

    print_to_file(left_dict,outf1);
    
    print_to_file(right_dict,outf2);
    
    print_to_file(overlap, inspect);
    
    print_to_file(same_diffs, same_event)
        

if __name__ == '__main__':
    main();


