#!/bin/bash

run_tcpcsm() {
	workingdir=$PWD
	tracefile=$1
	tmpdir=$2

	cd $tmpdir
	echo "executing tcpcsm on $tracefile, writing to $tracefile.csm at $tmpdir"
	tcpcsm -R -o ./tcpcsm.out $tracefile
	cd $workingdir
}

run_bro() {
	workingdir=$PWD
	tracefile=$1
	tmpdir=$2

	cd $tmpdir
	echo "executing bro on $tracefile, writing script results to $tmpdir"
	bro -Cr $tracefile policy/protocols/tcp/tcp_analysis
	cd $workingdir
}

separate_tcpcsm_logs() {
	workingdir=$PWD
	csmdir=$1
		
	cd $csmdir
	for each in `awk '{print $7}' tcpcsm.out | sort | uniq` 
	do
		egrep [[:space:]]$each[[:space:]] tcpcsm.out > $each.raw
	done
	cd $workingdir 
}

main() {
	tracefile=$(readlink -f $1)
	dest=$(readlink -f $2)
	tmpdir=`mktemp -d`
	PATH=${PATH}:/usr/local/bro/bin:/usr/sbin
	export PATH
	
	# ensure directories exist
	if [[ ! -d "$dest" ]]
	then
		echo "mkdir $dest" 
		mkdir $dest
	fi
	
	if [[ ! -d "$dest/tcprs" ]]
	then
		echo "mkdir $dest/tcprs"
		mkdir $dest/tcprs
	fi	

	if [[ ! -d "$dest/tcpcsm" ]]
	then
		echo "mkdir $dest/tcpcsm"
		mkdir $dest/tcpcsm
	fi 

	if [[ ! -f "$tracefile" ]] 
	then 
		echo "$tracefile does not exist ... exiting"
		exit 1
	fi

	#execute each program on the given tracefile	
	run_tcpcsm $tracefile $dest/tcpcsm
	run_bro $tracefile $dest/tcprs

	separate_tcpcsm_logs $dest/tcpcsm
}

usage() {
        echo "Usage: generate_raw_output.sh <tracefile> <destination_directory>"
}

# start of program
args=$#

if [[ "$args" -eq "2" ]]
then
        main $1 $2
else
        usage
fi


