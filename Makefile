TARGETS     = $(wildcard *.sh) $(wildcard *.py) $(wildcard *.awk)
SUBDIRS     =
INSTALL_DIR=/usr/local/bin/tcprs/scripts

all: recursive-all $(TARGETS)

recursive-all: $(SUBDIRS)

$(TARGETS)::
	@mkdir -p ${INSTALL_DIR}
	@chmod 755 ${INSTALL_DIR}
	cp $@ ${INSTALL_DIR}
	chmod 755 ${INSTALL_DIR}/$@

$(SUBDIRS)::
	$(MAKE) -C $@ $(MAKECMDGOALS)

clean: recursive-clean
	rm -f $(REMOVE)

recursive-clean: $(SUBDIRS)

.PHONY: clean

