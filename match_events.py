#!/usr/bin/env python

from optparse import OptionParser, make_option
from sys import exit
from os import sep
from os.path import dirname,abspath,basename,exists
import re;

tcprs_regex = re.compile("^[0-9]+");

def read_in_tcprs_events(events, logs):
	''' read  in each record from the logs to match against ''' 
	for logfile in logs:
		with open(logfile) as lf:
			for event in lf:
				if (not tcprs_regex.match(event)):
					continue;
				
				fields = event.rstrip().split();
				conn_id = " ".join([fields[4],fields[2],fields[5],
									fields[3]]);
				ts = fields[0];				
				
				if (conn_id not in events):
					events[conn_id] = {};
					events[conn_id][ts] = [];
					
				if (ts not in events[conn_id]):
					events[conn_id][ts] = [];
					
				events[conn_id][ts].append(event.rstrip());
									
	return;
	
def read_in_tcpcsm_events(events, logs):
	''' read  in each record from the logs to match against ''' 
	for logfile in logs:
		with open(logfile) as lf:
			for event in lf:
				fields = event.rstrip().split();
				conn_id = " ".join(fields[0:4]);
				ts = fields[7];
				
				if (conn_id not in events):
					events[conn_id] = {};
					events[conn_id][ts] = [];
					
				if (ts not in events[conn_id]):
					events[conn_id][ts] = [];
					
				events[conn_id][ts].append(event.rstrip());
				
	return;

def init_parser():
    usage="usage: %prog [options] source_program.dfr logfile_directory logfilelist tracefile"
    options_list = [];
    parser = OptionParser(usage=usage, option_list=options_list);
    return parser;

def main():
	parser = init_parser();
    
	(options, args) = parser.parse_args();
    
	src = None;
	dst = None;
    
	if (len(args) != 4):
		parser.print_help();
		exit(-1)
        
	''' read in the list of files corresponding to the source 
		application '''     
   
	outfile = None;
	
	
	src_filepath = abspath(args[0]);
	src_directory = dirname(src_filepath);
	filename = basename(src_filepath).split(".")[0]
	src_program = "tcpcsm"
	dst_program = "tcprs"
	
	if(filename.find("tcprs") != -1):
		src_program = "tcprs";
		dst_program = "tcpcsm";
	
	try:
		src = open(args[0]);
	except IOError:
		print "could not open {0}".format(args[0])
		exit(1);
		
	try:
		if (not exists(args[1])):
			raise Exception("couldn't locate directory {0}".format(args[2]));
	except Exception as e:
		print e;
		exit(1);
		
	log_directory=args[1];
	tracefile=abspath(args[3]);
		
	log_files = [];

	for file in args[2].split(","):
		if (not exists(log_directory + sep + file)):
			print "{0} does not exist".format(log_directory + sep + file);
		else:
			log_files.append(abspath(log_directory + sep + file));
		
	outfile = open(src_directory + sep + filename + ".ldr","w");
	
	outfile.write("#!/bin/bash\n");
	outfile.write("#< event in {0}\n".format(src_program));
	outfile.write("#> event in {0}\n".format(dst_program));
	outfile.write("# tcpdump command\n");
	outfile.write("if [[ ! -d {0}/traces ]] ; then mkdir {0}/traces ; fi\n".format(src_directory));
	outfile.write("if [[ ! -d {0}/plots ]] ; then mkdir {0}/plots ; fi\n".format(src_directory));
	
	event_dict = {};
	
	if (src_program is "tcpcsm"):
		read_in_tcprs_events(event_dict, log_files);
	else:
		read_in_tcpcsm_events(event_dict, log_files);
		
	''' read in each record from the source program '''
	for record in src:
		outfile.write("\n#< {0}".format(record));
		fields = record.split();
		conn_id = " ".join(fields[0:4]);
		ts = fields[5];
		
		if (conn_id in event_dict and ts in event_dict[conn_id]):
			for event in event_dict[conn_id][ts]:
				outfile.write("#> {0}\n".format(event));
			
		outfile.write("/usr/sbin/tcpdump -r {0} -w {1}/traces/{2}_{3}_{4}_{5}.dmp \"host {2} and host {3} and port {4} and port {5}\"\n".format(tracefile,src_directory,fields[0],fields[1],fields[2],fields[3]));

if __name__ == '__main__':
    main();



