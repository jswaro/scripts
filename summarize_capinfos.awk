#!/usr/bin/awk -f 

BEGIN {
	packets = 0;
	duration = 0;
}

{
	if ($1 == "Number" && $2 == "of" && $3 == "packets:") {
		packets += $4
	} else if ($1 == "Capture" && $2 == "duration:") {
		duration += $3
	}
}

END {
	print ("Number of packets: " packets)
	print ("Capture duration(in seconds): " duration)
}
