#!/bin/bash


check_tcprs_files() {
	basedir=$1
	tcprsdir=$basedir/tcprs
	for each in conn.log tcpreordering.log 
	do
		if [[ ! -f "$tcprsdir/$each" ]] 
		then 
			echo "$tcprsdir/$each does not exist ... critical file missing"
			exit 1
		fi
	done
}

check_tcpcsm_files() {
	basedir=$1
	workingdir=$basedir/tcpcsm
	for each in tcpcsm.out 
	do
		if [[ ! -f "$workingdir/$each" ]]
		then
				echo "$workingdir/$each does not exist ... critical file missing"
				exit 1
		fi
	done

}

# This function creates equivalence files for comparison against TCPRS.
#   results from this will have the .res file suffix
# 
#  flow_id: <destination host> <source host> <destination port> <source port> 
#		<id number>
#
#  REORDER: <flow_id> <dir> REORDER <timestamp>
#
create_csm_equivfiles() {
	basedir=$1
	csmdir=$basedir/tcpcsm
	workingdir=$PWD
	
	cd $csmdir
	
	echo "creating tcpcsm equiv(*.res) event files from raw tcpcsm output"

	egrep [[:space:]]'FRETX|MS_FRETX|SACK_FRETX|BAD_FRETX|REORDER|RTO|LINK_DUP|LOSS_REC|FREC|UNEXP_FREC|RETX_OVERLAP|RETX_NEW|UNNEEDED|IPID_DUP'[[:space:]] $csmdir/tcpcsm.out > $csmdir/tcpcsm.eqv
	
	for logfile in $csmdir/*.raw
	do
		fname=$(basename $logfile)
		type=`echo $fname | awk -F. '{print $1}'`
		case $fname in
			REORDER.raw)
				# NOTHING NEEDS DONE HERE 
				awk '{print $1, $2, $3, $4, $7, $8}' $logfile | sort -s > $csmdir/$type.res	;;
			RTO.raw)
				# NOTHING NEEDS DONE HERE 
				awk '{print $1, $2, $3, $4, $7, $8}' $logfile | sort -s > $csmdir/$type.res	;;
			FRETX.raw)
				egrep [[:space:]]'FRETX|MS_FRETX|SACK_FRETX|BAD_FRETX'[[:space:]] $csmdir/tcpcsm.out > $logfile
				#awk '{print $1, $2, $3, $4, "FRETX"}' $logfile | sort -s > $csmdir/$type.res ;;
				awk '{print $1, $2, $3, $4, "FRETX", $8}' $logfile | sort -s > $csmdir/$type.res ;;
			*)
				continue ;;
		esac
	done
	
	cd $workingdir
}

create_rs_equivfiles() {
	basedir=$1
	rsdir=$basedir/tcprs
	workingdir=$PWD
	
	cd $rsdir
	
	echo "creating tcpcsm equiv(*.res) event files from raw tcprs output"


	
	for logfile in $rsdir/tcp*.log 
	do
		
		case $(basename $logfile) in
			"tcposfp.log") # no equivalent records in tcpcsm
				continue ;; 
			"tcprtt.log") # no equivalent records in tcpcsm
				continue ;;
			"tcpretransmissions.log")
				# create a human readable version of the file
				filter_bad_records $logfile > $logfile.safe
				
				readtcplog < $logfile.safe > $logfile.hr
				
				# make RTO file
				egrep ^[0-9]+  $logfile.hr | egrep [[:space:]]'RTO|RTO_NO_DUP_ACK|BRUTE_FORCE_RTO'[[:space:]] $logfile.hr > $rsdir/RTO.raw 
				# format: <dst_h> <src_h> <dst_p> <src_p> RTO <ts>
				awk '{print $5,$3,$6,$4,"RTO",$1}' $rsdir/RTO.raw | sort -s > $rsdir/RTO.res
				
				## create the fast retransmission equivalence files
				egrep ^[0-9]+ $logfile.hr | egrep 'FAST_SUSPECT|FAST_3DUP|EARLY_REXMIT|SEGMENT_EARLY_REXMIT|SACK_SEGMENT_EARLY_REXMIT|SACK_BYTE_EARLY_REXMIT|BYTE_EARLY_REXMIT|SACK_BASED_RECOVERY|FACK_BASED_RECOVERY' > $rsdir/FRETX.raw
				# format: <dst_h> <src_h> <dst_p> <src_p> FRETX <ts>
				#awk '{print $5,$3,$6,$4,"FRETX"}' $rsdir/FRETX.raw| sort -s > $rsdir/FRETX.res ;;
				awk '{print $5,$3,$6,$4,"FRETX",$1}' $rsdir/FRETX.raw| sort -s > $rsdir/FRETX.res ;;

			"tcpreordering.log")
				filter_bad_records $logfile > $logfile.safe
				readtcplog < $logfile.safe > $logfile.hr
				
				egrep ^[0-9]+ $logfile.hr > $rsdir/REORDER.raw
				
				# format: <dst_h> <src_h> <dst_p> <src_p> REORDER <ts>
				awk '{print $5,$3,$6,$4,"REORDER",$1}' $rsdir/REORDER.raw | sort -s > $rsdir/REORDER.res ;;
			*)
				continue ;;
				
		esac
	done
	cd $workingdir
}

diff_equivfiles() {
	workingdir=$PWD
	basedir=$1
	resultsdir=$basedir/results
	tmpdir=$resultsdir/tmp	
	tracefile=$2


	if [[ ! -d "$resultsdir" ]]
	then 
		mkdir $resultsdir
	fi

	if [[ ! -d "$tmpdir" ]]
	then
		mkdir $tmpdir
	fi


	# Take each file and break it down into the respective diffs. 
	# Once that is done, split the diffs based on the program
	#   Then, strip the diff markers and leading whitespace so the
	#   files can be read into python for approximate matching that
	#   isn't so easily done in shell scripts
	for each in REORDER RTO FRETX
	do		
		
		case $each in 
			
			REORDER)
				tcprs_type_file="tcpreordering.log" 
				tcprs_file_list="tcpreordering.log.hr tcpretransmissions.log.hr";;
			RTO|FRETX)
				tcprs_type_file="tcpretransmissions.log"
				tcprs_file_list="tcpreordering.log.hr tcpretransmissions.log.hr";;
			*)
				echo "oops -- $each is not yet defined for analysis"
				exit 0 ;; 
		esac
		
		eventdir=$resultsdir/$each
		plotscript=$eventdir/$each.dmpscript
		
		echo "processing $each events"

		if [[ ! -d "$eventdir" ]] 
		then
			mkdir $eventdir
		fi

		diff -i -E -b -B -w -d $basedir/tcprs/$each.res $basedir/tcpcsm/$each.res | grep ^[\<\>] > $tmpdir/$each.diff.preprocess
		egrep ^\< $tmpdir/$each.diff.preprocess > $tmpdir/tcprs.$each.diff.preprocess
		egrep ^\> $tmpdir/$each.diff.preprocess > $tmpdir/tcpcsm.$each.diff.preprocess
		awk '{$1=""; print $0}' $tmpdir/tcprs.$each.diff.preprocess | sed -e 's/^[ \t]*//' > $tmpdir/$each.tcprs
		awk '{$1=""; print $0}' $tmpdir/tcpcsm.$each.diff.preprocess | sed -e 's/^[ \t]*//' > $tmpdir/$each.tcpcsm
	
		cp $tmpdir/$each.tcprs $tmpdir/$each.tcprs.dfr 
		cp $tmpdir/$each.tcpcsm $tmpdir/$each.tcpcsm.dfr

		sort -s $tmpdir/$each.tcpcsm.dfr > $eventdir/tcpcsm.dfr
		sort -s $tmpdir/$each.tcprs.dfr > $eventdir/tcprs.dfr
		cp $basedir/tcprs/$each.res $eventdir/tcprs.res
		cp $basedir/tcpcsm/$each.res $eventdir/tcpcsm.res		
		cp $tmpdir/$each.tcprs $eventdir/$each.tcprs
		cp $tmpdir/$each.tcpcsm $eventdir/$each.tcpcsm
		
		# create linked diff reports for each program 
		echo "creating linked diff report of tcpcsm for $each"
	
		match_events.py $eventdir/tcpcsm.dfr $basedir/tcprs `echo $tcprs_file_list | sed -e 's/ /,/g'` $tracefile
		
		echo "creating linked diff report of tcprs for $each"
			
		match_events.py $eventdir/tcprs.dfr $basedir/tcpcsm tcpcsm.eqv $tracefile
		
	done
}


create_info_file() {
	workingdir=$PWD
	basedir=$1
	resultsdir=$basedir/results
	tmpdir=$resultsdir/tmp
	eventdir=$resultsdir/REORDER
	resultfile=${resultsdir}/REORDER.info
	event="REORDER RTO FRETX"

	for type in $event
	do
		echo "creating info file for $type events"
		eventdir=$resultsdir/$type
		resultsfile=$resultsdir/$type.info
		
		if [[ -f $resultsfile ]] 
		then 
			rm $resultsfile
		fi 
		
		touch $resultsfile
		
		for program in tcprs tcpcsm
		do
			count=`wc -l ${eventdir}/${program}.res | awk '{print $1}'`
			diffs=`wc -l ${eventdir}/${program}.dfr | awk '{print $1}'`
			
			echo "$program.$type.count: $count" >> $resultsfile
			echo "$program.$type.diffs: $diffs" >> $resultsfile
			
			
			event_count=$(wc -l ${eventdir}/${program}.dfr| awk '{print $1}')
			
			unknown_count=0
			#create the event distribution
			if [[ "$program" == "tcpcsm" ]] 
			then
				egrep "#> "[0-9]+ $eventdir/$program.ldr | awk '{if($8 == "TCP::Reordering" && $14 == "True") print "AMBIGUOUS_REORDERING"; else if($8 == "TCP::Reordering" && $14 == "False") print "REORDERING" ; else print $14}' | sort | uniq -c | awk -v program=$program -v type=$type '{print program"."type".TCPRS."$2": "$1}' >> $resultsfile
				unknown_count=$((${event_count} - $(egrep "#> "[0-9]+ $eventdir/${program}.ldr | wc -l | awk '{print $1}')))
				echo "$program.$type.TCPRS.no_event: ${unknown_count}" >> $resultsfile
			else
				egrep "#> "[0-9]+ $eventdir/$program.ldr > $eventdir/.tmp
				egrep [[:space:]]'RTO|FRETX|MS_FRETX|SACK_FRETX|BAD_FRETX|LOSS_REC|FREC|UNEXP_FREC|RETX_OVERLAP|RETX_NEW|UNNEEDED|REORDER|LINK_DUP|IPID_DUP'[[:space:]] $eventdir/.tmp > $eventdir/.processed
				cat $eventdir/.processed | awk '{print $8}' | sort | uniq -c | awk -v program=$program -v type=$type '{print program"."type".TCPCSM."$2": "$1}' >> $resultsfile
				unknown_count=$((${event_count} - $(cat $eventdir/.processed | wc -l | awk '{print $1}')))
				echo "$program.$type.TCPCSM.no_event: ${unknown_count}" >> $resultsfile
			fi
		done
	done
	
}

# This script expects that tcpcsm and bro have been run on a tracefile. 
#   The expectation is that the results are stored at $basedir/tcprs
#   and $basedir/tcpcsm

main() {
	bad=1
	basedir=$(readlink -f $1)
	tracefile=$(readlink -f $2)
	PATH=${PATH}:/usr/local/bro/bin:/usr/sbin:/usr/local/bin
	export PATH

	# ensure directories exist
	if [[ ! -d "$basedir" ]]
	then
		echo "$basedir does not exist ... exiting"
	retcode=1
	fi

	if [[ ! -d "$basedir/tcprs" ]]
	then
			echo "$basedir/tcprs does not exist ... exiting"
			retcode=1
	fi

	if [[ ! -d "$basedir/tcpcsm" ]]
	then
			echo "$basedir/tcpcsm does not exist ... exiting"
			retcode=1
	fi

	if [[ "$retcode" -eq "$bad" ]] 
	then
		exit 1
	fi


	#ensure critical files exist
	check_tcprs_files $basedir
	check_tcpcsm_files $basedir

	create_csm_equivfiles $basedir
	create_rs_equivfiles $basedir

	diff_equivfiles $basedir $tracefile

	create_info_file $basedir
}



usage() {
        echo "Usage: compare_reordering <base_directory> <tracefile>"
}

# start of program
args=$#

if [[ "$args" -eq "2" ]]
then
        main $1 $2
else
        usage
fi

