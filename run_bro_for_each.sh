#!/bin/bash

WORKING=$PWD


echo "#!/bin/bash" > $WORKING/make_filter.sh
echo "head -n8 \$1 > .tmp" >> $WORKING/make_filter.sh 
egrep "^#< [0-9]+" $WORKING/gen_traces.sh | awk '{ print "egrep \"^"$7".*"$3"[[:space:]]"$5"[[:space:]]"$2"[[:space:]]"$4"\" $1 >> .tmp"}' >> $WORKING/make_filter.sh
echo "tail -n1 \$1 >> .tmp" >> $WORKING/make_filter.sh
echo "mv .tmp \$1" >> $WORKING/make_filter.sh

chmod +x $WORKING/make_filter.sh

for trace in $@ ; do 
	tfile=`basename $trace`
	mkdir -p $WORKING/bro/$tfile
	
	cd $WORKING/bro/$tfile
	echo "running bro against $WORKING/traces/$tfile"
	bro -Cr $WORKING/traces/$tfile policy/protocols/tcp/tcp_analysis

	echo $PWD
	for logfile in tcpretransmissions.log tcpreordering.log ; do 
		if [[ ! -f $logfile ]] ; then
			echo "couldn't find the file $logfile"
			continue
		fi

		cp $logfile $logfile.old

		bash $WORKING/make_filter.sh $logfile
	done
	cd $WORKING

	mkdir -p $WORKING/xpl/$tfile
	tcptrace --output_prefix=${tfile}_ --xplot_title_prefix=${tfile}_ --output_dir=$WORKING/xpl/$tfile -nS $trace
	annotate -d $WORKING/bro/$tfile -p $WORKING/xpl/$tfile 
	
done

cd $WORKING
