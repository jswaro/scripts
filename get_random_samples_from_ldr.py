#!/usr/bin/python
import sys
import os
import random
from optparse import OptionParser, make_option


class diff_record(object):
    def __init__(self):
        self.id = "UNKNOWN"
        self.src_record = []
        self.dst_record = []
        self.dump_stmt = ""

def is_picked(picked, record_set):
    for record in record_set:
        if record in picked:
            return False
    return True

def choose_records(records, count):
    if len(records) <= count:
        return records

    records_to_choose_from = list()
    for r in records:
        records_to_choose_from.append(r)

    picked_records = set([])
    
    for x in xrange(0, count):
        r = random.choice(records_to_choose_from)
        records_to_choose_from.remove(r)
        picked_records.add(r)
        
    return picked_records

    
def print_choices(choices, outfile):
    dmps = []
    count = 0
    print >> outfile, "#!/bin/bash"
    for record in choices:
        print >> outfile, "\n".join(record.src_record)
        print >> outfile, "\n".join(record.dst_record)
    
        if record.dump_stmt not in dmps:
            trace_file = record.dump_stmt.split()[4]
            print >> outfile, "mkdir -p {0}".format(os.path.dirname(trace_file))
            print >> outfile, record.dump_stmt
            print >> outfile, "mv {0} {1}.trace".format(trace_file, str(count).zfill(3))
            dmps.append(record.dump_stmt)
            count += 1
        else:
            print >> outfile, "## trace already taken care of"
        
    print >> outfile, "for x in *.trace; do tcptrace --output_prefix=$x. --xplot_title_prefix=$x -nS $x ; done"

def init_parser():
    usage = "usage: %prog [options] <ldr>"
    options_list = [make_option("-s", "--seed", dest="seed",
                        help="sets the seed for the random number generator",
                        default=0xDEADBEEF),
		    make_option("-i", "--in", dest="infile",
                        help="file to read from",
                        default=None),
		    make_option("-o", "--out", dest="outfile",
                        help="file to write to",
                        default=None)];
    parser = OptionParser(usage=usage, option_list=options_list);    
    return parser;

def main():
    parser = init_parser();
    (options, args) = parser.parse_args();
    random.seed(options.seed);
    
    infile = sys.stdin
    outfile = sys.stdout
    
    if options.infile is not None:
    	infile = open(options.infile, 'r')
    
    if options.outfile is not None:
    	outfile = open(options.outfile, 'w')
    
    records = []
    
    r = None
    
    header = ""
    ''' strip the file header '''
    for x in xrange(0, 6):
        header += infile.readline()
        
    for line in infile:
        if line.startswith("#<"):
            r = diff_record()
            r.src_record.append(line.strip())
        elif line.startswith("#>"):
            r.dst_record.append(line.strip())
        elif line.startswith("/usr/sbin/tcpdump"):
            r.dump_stmt = line.strip()
            records.append(r)
            r = None
    		
    if infile != sys.stdin:
    	infile.close()
    
    count = 100
    choices = choose_records(records, count)
    print_choices(choices, outfile)
    
    if outfile != sys.stdout:
            outfile.close()

if __name__ == "__main__":
    main()
