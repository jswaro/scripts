#!/usr/bin/env python

from optparse import OptionParser, make_option
from sys import exit
from os import sep
from os.path import dirname,abspath,basename,exists
import re
import sys

class diff_record(object):
	def __init__(self):
		self.src_records = []
		self.dst_records = []
		self.dump_stmt = ""
		self.type = "None"

def init_parser():
    usage="usage: %prog [options] <ldr>"
    options_list = [];
    parser = OptionParser(usage=usage, option_list=options_list);
    return parser;

def main():
	parser = init_parser();
    
	(options, args) = parser.parse_args();
    
	if len(args) < 1:
		parser.print_help()
		exit(1)


	records = {}

	with open(sys.argv[1],'r') as log:
		for x in xrange(0,6):			
			trash = log.readline()

		r = None
		for line in log:
			if line.startswith("#<"):
				if r is not None:
					if r.type not in records.keys():
						records[r.type] = []
					records[r.type].append(r)
				r = diff_record()
				
				r.src_records.append(line.strip())
			elif line.startswith("#>"):
				if r.type == "None":
					r.type = line.split()[7]
				r.dst_records.append(line.strip())
			elif line.startswith("/usr/sbin/tcpdump"):
				r.dump_stmt = line.strip();
			else:
				pass


	for cls in records.keys():
		d = records[cls]
		with open("{0}.ldr".format(cls),'w') as log:
			for entry in d:
				for i in entry.src_records:
					print >> log, i
				for i in entry.dst_records:
					print >> log, i
				print >> log, entry.dump_stmt
				print >> log, ""

if __name__ == '__main__':
    main();



